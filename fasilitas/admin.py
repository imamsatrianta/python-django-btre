from django.contrib import admin

# Register your models here.
from .models import Fasilitas

class FasilitasModel(admin.ModelAdmin):
  list_display = ('id', 'title', 'luas_m','is_published')
  list_display_links = ('id', 'title')

  list_per_page = 25

admin.site.register(Fasilitas, FasilitasModel)