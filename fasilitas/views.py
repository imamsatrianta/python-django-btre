from django.shortcuts import get_object_or_404, render
from django.core.paginator import EmptyPage, Paginator, PageNotAnInteger
from .models import Fasilitas
# Create your views here.


def index(request):
    fasilitass = Fasilitas.objects.order_by('-list_date').filter(is_published=True)
    paginator = Paginator(fasilitass, 6)
    page = request.GET.get('page')
    paged_fasilitass = paginator.get_page(page)

    context = {
        'listings':paged_fasilitass
    }
    return render(request, 'fasilitas/fasilitass.html', context)


def fasilitas(request, listing_id):
    listing = get_object_or_404(Fasilitas, pk=listing_id)
    context = {
        'listing':listing
    }
    return render(request, 'listings/listing.html', context)

def lapangan(request):
    return render(request, 'fasilitas/lapangan.html')

def search(request):
    return render(request, 'listings/search.html')
