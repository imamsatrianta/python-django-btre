from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='fasilitas' ),
    path('lapangan', views.lapangan, name='lapangan' ),
    path('<int:fasilitas_id>', views.fasilitas, name='fasilitas'),
    path('search', views.search, name='search'),

]