from django.apps import AppConfig


class FasilitasConfig(AppConfig):
    name = 'fasilitas'
