from django.shortcuts import render
from django.http import HttpResponse
from guru.models import Guru
from realtors.models import Realtor

# Create your views here.
def index(request):
    gurus = Guru.objects.order_by('-username').filter(is_published=True)[:3]
    context = {
        'gurus':gurus
    }
    return render(request, 'pages/index.html', context)

def about(request):
    realtors = Realtor.objects.order_by('-hire_date')
    
    #Get Mvp
    mvp_realtors = Realtor.objects.all().filter(is_mvp=True)
    context = {
        'realtors':realtors,
        'mvp_realtors':mvp_realtors,
    }
    return render(request, 'pages/about.html', context)
    