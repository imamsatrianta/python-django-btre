from django.contrib import admin

# Register your models here.
from .models import Gallery

class GalleryAdmin(admin.ModelAdmin):
  list_display = ('id', 'title', 'subtitle','is_published')
  list_display_links = ('id', 'title')
  search_fields = ('title', 'description')
  list_editable = ('is_published',)
  list_per_page = 25

admin.site.register(Gallery, GalleryAdmin)