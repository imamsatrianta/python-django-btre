from django.db import models
from datetime import datetime

# Create your models here.
class Guru(models.Model):
    username = models.CharField(max_length = 200)
    address = models.CharField(max_length = 200)
    city = models.CharField(max_length = 100)
    state = models.CharField(max_length = 100)
    zipcode = models.CharField(max_length = 100)
    description = models.TextField(blank = True)
    study = models.CharField(max_length = 100,blank=True)
    photo_main = models.ImageField(upload_to='photos/%Y/%m/%d/')
    is_published = models.BooleanField(default=True)
    created_date = models.DateTimeField(default=datetime.now, blank=True)
    def __str__(self):
        return self.username