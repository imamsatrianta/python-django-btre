from django.contrib import admin

# Register your models here.
from .models import Guru

class GuruAdmin(admin.ModelAdmin):
  list_display = ('id', 'username', 'study','is_published')
  list_display_links = ('id', 'username')
  search_fields = ('username', 'study')
  list_editable = ('is_published',)
  list_per_page = 25

admin.site.register(Guru, GuruAdmin)